package top.hmtools.wxmp.account.apis;

import static org.junit.Assert.fail;

import java.io.InputStream;

import org.junit.Before;
import org.junit.Test;

import top.hmtools.wxmp.AppId;
import top.hmtools.wxmp.account.enums.EQRActionName;
import top.hmtools.wxmp.account.models.ActionInfo;
import top.hmtools.wxmp.account.models.QRCodeParam;
import top.hmtools.wxmp.account.models.QRCodeResult;
import top.hmtools.wxmp.account.models.Scene;
import top.hmtools.wxmp.account.models.TicketParam;
import top.hmtools.wxmp.core.WxmpSession;
import top.hmtools.wxmp.core.WxmpSessionFactory;
import top.hmtools.wxmp.core.WxmpSessionFactoryBuilder;
import top.hmtools.wxmp.core.configuration.WxmpConfiguration;

public class IQrcodeApisTest {
	
	protected WxmpSession wxmpSession;
	private IQrcodeApis qrcodeApis ;
	
	@Before
	public void init(){
		WxmpConfiguration wxmpConfiguration = new WxmpConfiguration();
		wxmpConfiguration.setAppid(AppId.appid);
		wxmpConfiguration.setAppsecret(AppId.appsecret);
		WxmpSessionFactoryBuilder builder = new WxmpSessionFactoryBuilder();
		WxmpSessionFactory factory = builder.build(wxmpConfiguration);
		this.wxmpSession = factory.openSession();
		qrcodeApis = this.wxmpSession.getMapper(IQrcodeApis.class);
	}

	@Test
	public void testCreate() {
		QRCodeParam codeParam = new QRCodeParam();
		
		ActionInfo action_info = new ActionInfo();
		Scene scene = new Scene();
		scene.setScene_str("aaaabbbbcccc");
		action_info.setScene(scene);
		codeParam.setAction_info(action_info);
		
		codeParam.setAction_name(EQRActionName.QR_STR_SCENE);
		
		codeParam.setExpire_seconds(20*60*60*60l);
		QRCodeResult create = this.qrcodeApis.create(codeParam);
		System.out.println(create);
	}

	@Test
	public void testShowQrCode() {
		TicketParam ticketParam = new TicketParam();
		ticketParam.setTicket("gQEy8DwAAAAAAAAAAS5odHRwOi8vd2VpeGluLnFxLmNvbS9xLzAyODQtVVlPcG5kQ2oxd09SMmh0Y3MAAgQyqFpdAwQAjScA");
		InputStream showQrCode = this.qrcodeApis.showQrCode(ticketParam);
		System.out.println(showQrCode);
	}

}
