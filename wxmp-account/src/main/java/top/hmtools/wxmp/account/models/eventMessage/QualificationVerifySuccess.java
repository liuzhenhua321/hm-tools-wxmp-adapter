package top.hmtools.wxmp.account.models.eventMessage;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import top.hmtools.wxmp.core.model.message.BaseEventMessage;

/**
 * 资质认证成功（此时立即获得接口权限）
 * <br>
 * <p>
 * {@code
 * <xml>
  <ToUserName><![CDATA[toUser]]></ToUserName>  
  <FromUserName><![CDATA[fromUser]]></FromUserName>  
  <CreateTime>1442401156</CreateTime>  
  <MsgType><![CDATA[event]]></MsgType>  
  <Event><![CDATA[qualification_verify_success]]></Event>  
  <ExpiredTime>1442401156</ExpiredTime> 
</xml>
 * }
 * 
</p>
 * @author HyboWork
 *
 */
public class QualificationVerifySuccess extends BaseEventMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6833759294968156598L;
	
	@XStreamAlias("ExpiredTime")
	private Long expiredTime;
	
	

	public Long getExpiredTime() {
		return expiredTime;
	}



	public void setExpiredTime(Long expiredTime) {
		this.expiredTime = expiredTime;
	}



	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	

	@Override
	public String toString() {
		return "QualificationVerifySuccess [expiredTime=" + expiredTime + ", event=" + event + ", eventKey=" + eventKey
				+ ", toUserName=" + toUserName + ", fromUserName=" + fromUserName + ", createTime=" + createTime
				+ ", msgType=" + msgType + ", msgId=" + msgId + "]";
	}



	@Override
	public void configXStream(XStream xStream) {

	}

}
