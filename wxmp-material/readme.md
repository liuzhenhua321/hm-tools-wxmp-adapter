#### 前言
本组件对应实现微信公众平台“素材管理”章节相关api接口，原接口文档地址：[素材管理](https://developers.weixin.qq.com/doc/offiaccount/Asset_Management/New_temporary_materials.html)

#### 接口说明
- `top.hmtools.wxmp.material.apis.ITemporaryApi` 对应临时素材相关接口
- `top.hmtools.wxmp.material.apis.IForeverApi` 	对应永久素材相关接口

#### 事件消息类说明

#### 使用示例
```
```
更多示例参见：
- [临时素材](src/test/java/top/hmtools/wxmp/material/apis/ITemporaryApiTest.java)
- [永久素材](src/test/java/top/hmtools/wxmp/material/apis/IForeverApiTest.java)