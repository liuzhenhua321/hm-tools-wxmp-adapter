package top.hmtools.wxmp.material.apis;

import static org.junit.Assert.fail;

import java.io.File;
import java.io.InputStream;

import org.junit.Before;
import org.junit.Test;

import top.hmtools.wxmp.AppId;
import top.hmtools.wxmp.core.WxmpSession;
import top.hmtools.wxmp.core.WxmpSessionFactory;
import top.hmtools.wxmp.core.WxmpSessionFactoryBuilder;
import top.hmtools.wxmp.core.configuration.WxmpConfiguration;
import top.hmtools.wxmp.material.enums.MediaType;
import top.hmtools.wxmp.material.model.MediaBean;
import top.hmtools.wxmp.material.model.MediaParam;
import top.hmtools.wxmp.material.model.UploadParam;
import top.hmtools.wxmp.material.model.VideoResult;

public class ITemporaryApiTest {
	
	protected WxmpSession wxmpSession;
	
	ITemporaryApi temporaryApi;
	
	@Before
	public void init(){
		WxmpConfiguration wxmpConfiguration = new WxmpConfiguration();
		wxmpConfiguration.setAppid(AppId.appid);
		wxmpConfiguration.setAppsecret(AppId.appsecret);
		WxmpSessionFactoryBuilder builder = new WxmpSessionFactoryBuilder();
		WxmpSessionFactory factory = builder.build(wxmpConfiguration);
		this.wxmpSession = factory.openSession();
		temporaryApi = this.wxmpSession.getMapper(ITemporaryApi.class);
	}

	@Test
	public void testUploadMedia() {
		File file = new File("E:\\tmp\\aaaa.png");
		if(!file.exists()){
			System.out.println(file+"文件不存在");
			return;
		}
		
		UploadParam uploadParam = new UploadParam();
		uploadParam.setMedia(file);
		uploadParam.setType(MediaType.image);
		MediaBean mediaBean = this.temporaryApi.uploadMedia(uploadParam);
		System.out.println(mediaBean);
	}

	@Test
	public void testGetImage() {
		MediaParam mediaParam = new MediaParam();
		mediaParam.setMedia_id("3IeZ4CwfoLQl2FMYvGe0ngFyDMzbbEYP6uzcutQz7fRmhA3llFT0Oul4hdXcO0R");
		InputStream image = this.temporaryApi.getImage(mediaParam);
	}

	@Test
	public void testGetVideo() {
		File file = new File("E:\\tmp\\bbb.mp4");
		if(!file.exists()){
			System.out.println(file+"文件不存在");
			return;
		}
		
		UploadParam uploadParam = new UploadParam();
		uploadParam.setMedia(file);
		uploadParam.setType(MediaType.video);
		MediaBean mediaBean = this.temporaryApi.uploadMedia(uploadParam);
		System.out.println(mediaBean);
		
		MediaParam mediaParam = new MediaParam();
		mediaParam.setMedia_id(mediaBean.getMedia_id());
		
		VideoResult video = this.temporaryApi.getVideo(mediaParam);
		System.out.println(video);
	}

	@Test
	public void testGetVoice() {
		fail("Not yet implemented");
	}

}
