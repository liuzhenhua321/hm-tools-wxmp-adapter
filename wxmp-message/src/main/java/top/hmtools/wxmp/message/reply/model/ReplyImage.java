package top.hmtools.wxmp.message.reply.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 回复图片消息 中 的图片信息
 * @author hybo
 *
 */
public class ReplyImage {

	@XStreamAlias("MediaId")
	private String mediaId;

	/**
	 * 通过素材管理中的接口上传多媒体文件，得到的id
	 * @return
	 */
	public String getMediaId() {
		return mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	@Override
	public String toString() {
		return "ReplyImage [MediaId=" + mediaId + "]";
	}
	
	
}
