package top.hmtools.wxmp.message.template.model.event;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import top.hmtools.wxmp.core.model.message.BaseEventMessage;

public class TemplateMessageSendedEvent extends BaseEventMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = -605661872549253228L;
	
	@XStreamAlias("MsgID")
	private String msgID;
	
	@XStreamAlias("Status")
	private String status;

	
	
	public String getMsgID() {
		return msgID;
	}



	public void setMsgID(String msgID) {
		this.msgID = msgID;
	}



	public String getStatus() {
		return status;
	}



	public void setStatus(String status) {
		this.status = status;
	}



	public static long getSerialversionuid() {
		return serialVersionUID;
	}



	@Override
	public String toString() {
		return "TemplateMessageSendedEvent [msgID=" + msgID + ", status=" + status + ", event=" + event + ", eventKey="
				+ eventKey + ", toUserName=" + toUserName + ", fromUserName=" + fromUserName + ", createTime="
				+ createTime + ", msgType=" + msgType + ", msgId=" + msgId + "]";
	}



	@Override
	public void configXStream(XStream xStream) {

	}

}
