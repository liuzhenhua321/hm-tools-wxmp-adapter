package top.hmtools.wxmp.message.group.model;

public class SpeedParam {

	/**
	 * 群发速度的级别，是一个0到4的整数，数字越大表示群发速度越慢。
	 * 
	 */
	private int speed;

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	@Override
	public String toString() {
		return "SpeedParam [speed=" + speed + "]";
	}
	
	
}
