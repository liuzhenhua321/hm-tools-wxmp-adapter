package top.hmtools.wxmp.message.group.model.preview;

public class WxcardPreviewParam extends BasePreviewParam {

	private Wxcard wxcard;

	public Wxcard getWxcard() {
		return wxcard;
	}

	public void setWxcard(Wxcard wxcard) {
		this.wxcard = wxcard;
	}

	@Override
	public String toString() {
		return "WxcardPreviewParam [wxcard=" + wxcard + ", msgtype=" + msgtype + ", touser=" + touser + "]";
	}
	
	
}
