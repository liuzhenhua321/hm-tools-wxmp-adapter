package top.hmtools.wxmp.user.model.eventMessage;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import top.hmtools.wxmp.core.model.message.BaseEventMessage;

/**
 * 获取用户地理位置 <br>
 * <p>
 * <xml> <ToUserName><![CDATA[toUser]]></ToUserName>
 * <FromUserName><![CDATA[fromUser]]></FromUserName>
 * <CreateTime>123456789</CreateTime> <MsgType><![CDATA[event]]></MsgType>
 * <Event><![CDATA[LOCATION]]></Event> <Latitude>23.137466</Latitude>
 * <Longitude>113.352425</Longitude> <Precision>119.385040</Precision> </xml>
 * </p>
 * 
 * @author HyboWork
 *
 */
public class LocationMessage extends BaseEventMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6822272150226654820L;
	/**
	 * 地理位置纬度
	 */
	@XStreamAlias("Latitude")
	protected double latitude;

	/**
	 * 地理位置经度
	 */
	@XStreamAlias("Longitude")
	protected double longitude;

	/**
	 * 地理位置精度
	 */
	@XStreamAlias("Precision")
	protected double precision;

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getPrecision() {
		return precision;
	}

	public void setPrecision(double precision) {
		this.precision = precision;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "LocationMessage [latitude=" + latitude + ", longitude=" + longitude + ", precision=" + precision
				+ ", event=" + event + ", eventKey=" + eventKey + ", toUserName=" + toUserName + ", fromUserName="
				+ fromUserName + ", createTime=" + createTime + ", msgType=" + msgType + ", msgId=" + msgId + "]";
	}

	@Override
	public void configXStream(XStream xStream) {

	}

}
