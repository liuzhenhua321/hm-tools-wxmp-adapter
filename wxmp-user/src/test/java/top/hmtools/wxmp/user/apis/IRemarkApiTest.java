package top.hmtools.wxmp.user.apis;

import org.junit.Before;
import org.junit.Test;

import top.hmtools.wxmp.AppId;
import top.hmtools.wxmp.core.WxmpSession;
import top.hmtools.wxmp.core.WxmpSessionFactory;
import top.hmtools.wxmp.core.WxmpSessionFactoryBuilder;
import top.hmtools.wxmp.core.configuration.WxmpConfiguration;
import top.hmtools.wxmp.core.model.ErrcodeBean;
import top.hmtools.wxmp.user.model.RemarkParam;

public class IRemarkApiTest {
	
	protected WxmpSession wxmpSession;
	private IRemarkApi remarkApi;
	
	@Before
	public void init(){
		WxmpConfiguration wxmpConfiguration = new WxmpConfiguration();
		wxmpConfiguration.setAppid(AppId.appid);
		wxmpConfiguration.setAppsecret(AppId.appsecret);
		WxmpSessionFactoryBuilder builder = new WxmpSessionFactoryBuilder();
		WxmpSessionFactory factory = builder.build(wxmpConfiguration);
		this.wxmpSession = factory.openSession();
		remarkApi = this.wxmpSession.getMapper(IRemarkApi.class);
	}

	@Test
	public void testUpdateRemark() {
		RemarkParam remarkParam = new RemarkParam();
		remarkParam.setOpenid("o2ddm028EcvP1GZGBZG_chnnpc1Y");
		remarkParam.setRemark("wahahahaahaah");
		ErrcodeBean updateRemark = this.remarkApi.updateRemark(remarkParam);
		System.out.println(updateRemark);
	}

}
