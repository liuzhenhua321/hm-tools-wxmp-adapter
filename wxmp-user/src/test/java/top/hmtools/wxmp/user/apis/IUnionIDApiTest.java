package top.hmtools.wxmp.user.apis;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import top.hmtools.wxmp.AppId;
import top.hmtools.wxmp.core.WxmpSession;
import top.hmtools.wxmp.core.WxmpSessionFactory;
import top.hmtools.wxmp.core.WxmpSessionFactoryBuilder;
import top.hmtools.wxmp.core.configuration.WxmpConfiguration;
import top.hmtools.wxmp.user.model.BatchUserInfoParam;
import top.hmtools.wxmp.user.model.BatchUserInfoResult;
import top.hmtools.wxmp.user.model.UserInfoParam;
import top.hmtools.wxmp.user.model.UserInfoResult;

public class IUnionIDApiTest {
	
	protected WxmpSession wxmpSession;
	private IUnionIDApi iUnionIDApi ;
	
	@Before
	public void init(){
		WxmpConfiguration wxmpConfiguration = new WxmpConfiguration();
		wxmpConfiguration.setAppid(AppId.appid);
		wxmpConfiguration.setAppsecret(AppId.appsecret);
		WxmpSessionFactoryBuilder builder = new WxmpSessionFactoryBuilder();
		WxmpSessionFactory factory = builder.build(wxmpConfiguration);
		this.wxmpSession = factory.openSession();
		iUnionIDApi = this.wxmpSession.getMapper(IUnionIDApi.class);
	}

	@Test
	public void testGetUserInfo() {
		UserInfoParam userInfoParam = new UserInfoParam();
		userInfoParam.setOpenid("o2ddm028EcvP1GZGBZG_chnnpc1Y");
		UserInfoResult userInfo = this.iUnionIDApi.getUserInfo(userInfoParam);
		System.out.println(userInfo);
	}

	@Test
	public void testGetBatchUserInfo() {
		BatchUserInfoParam batchUserInfoParam = new BatchUserInfoParam();
		
		UserInfoParam userInfoParam = new UserInfoParam();
		userInfoParam.setOpenid("o2ddm028EcvP1GZGBZG_chnnpc1Y");
		
		List<UserInfoParam> user_list = new ArrayList<>();
		user_list.add(userInfoParam);
		batchUserInfoParam.setUser_list(user_list);
		BatchUserInfoResult batchUserInfo = this.iUnionIDApi.getBatchUserInfo(batchUserInfoParam);
		System.out.println(batchUserInfo);
	}

}
