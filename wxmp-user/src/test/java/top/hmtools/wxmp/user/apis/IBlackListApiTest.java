package top.hmtools.wxmp.user.apis;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import top.hmtools.wxmp.AppId;
import top.hmtools.wxmp.core.WxmpSession;
import top.hmtools.wxmp.core.WxmpSessionFactory;
import top.hmtools.wxmp.core.WxmpSessionFactoryBuilder;
import top.hmtools.wxmp.core.configuration.WxmpConfiguration;
import top.hmtools.wxmp.core.model.ErrcodeBean;
import top.hmtools.wxmp.user.model.BatchBlackListParam;
import top.hmtools.wxmp.user.model.BlackListParam;
import top.hmtools.wxmp.user.model.BlackListResult;

public class IBlackListApiTest {
	
	protected WxmpSession wxmpSession;
	private IBlackListApi blackListApi;
	
	@Before
	public void init(){
		WxmpConfiguration wxmpConfiguration = new WxmpConfiguration();
		wxmpConfiguration.setAppid(AppId.appid);
		wxmpConfiguration.setAppsecret(AppId.appsecret);
		WxmpSessionFactoryBuilder builder = new WxmpSessionFactoryBuilder();
		WxmpSessionFactory factory = builder.build(wxmpConfiguration);
		this.wxmpSession = factory.openSession();
		blackListApi = this.wxmpSession.getMapper(IBlackListApi.class);
	}

	@Test
	public void testGetBlackList() {
		BlackListParam blackListParam = new BlackListParam();
		
		BlackListResult blackList = this.blackListApi.getBlackList(blackListParam);
		System.out.println(blackList);
	}

	@Test
	public void testBatchBlackList() {
		BatchBlackListParam batchBlackListParam = new BatchBlackListParam();
		List<String> openid_list = new ArrayList<>();
		openid_list.add("o2ddm028EcvP1GZGBZG_chnnpc1Y");
		batchBlackListParam.setOpenid_list(openid_list);
		ErrcodeBean batchBlackList = this.blackListApi.batchBlackList(batchBlackListParam);
		System.out.println(batchBlackList);
	}

	@Test
	public void testBatchUnblackList() {
		BatchBlackListParam batchBlackListParam = new BatchBlackListParam();
		List<String> openid_list = new ArrayList<>();
		openid_list.add("o2ddm028EcvP1GZGBZG_chnnpc1Y");
		batchBlackListParam.setOpenid_list(openid_list);
		ErrcodeBean batchUnblackList = this.blackListApi.batchUnblackList(batchBlackListParam);
		System.out.println(batchUnblackList);
	}

}
