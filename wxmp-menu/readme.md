#### 前言
本组件对应实现微信公众平台“自定义菜单”章节相关api接口，原接口文档地址：[自定义菜单](https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1434698695)

#### 接口说明
- `top.hmtools.wxmp.menu.apis.IMenuApi` 对应实现创建、查询、删除接口
- `top.hmtools.wxmp.menu.apis.IConditionalMenuApi` 对应实现个性化菜单接口相关

#### 事件消息类说明
- `top.hmtools.wxmp.menu.models.eventMessage`包下对应实现“事件推送”相关xml数据对应的Javabean数据结构。可参阅 top.hmtools.wxmp.menu.enums.EMenuEventMessages。

#### 使用示例
```
//1 设置所需的参数
WxmpConfiguration wxmpConfiguration = new WxmpConfiguration();
wxmpConfiguration.setAppid(AppId.appid);
wxmpConfiguration.setAppsecret(AppId.appsecret);

//2 获取动态代理对象实例
WxmpSessionFactoryBuilder builder = new WxmpSessionFactoryBuilder();
WxmpSessionFactory factory = builder.build(wxmpConfiguration);
WxmpSession wxmpSession = factory.openSession();
IMenuApi menuApi = wxmpSession.getMapper(IMenuApi.class);

//3 获取自定义菜单数据并打印
MenuWapperBean menu = menuApi.getMenu();
System.out.println(JSON.toJSONString(menu));
```
更多示例参见：
- [自定义菜单示例](src/test/java/top/hmtools/wxmp/menu/apis/IMenuApiTest.java)
- [个性化自定义菜单示例](src/test/java/top/hmtools/wxmp/menu/apis/IConditionalMenuApiTest.java)