package top.hmtools.wxmp.menu.models.eventMessage;

import org.junit.Test;

import top.hmtools.wxmp.core.model.message.enums.Event;
import top.hmtools.wxmp.core.model.message.enums.MsgType;

public class ClickEventMessageTest {

	@Test
	public void testFromStr() {
		String xmlStr = "<xml><ToUserName><![CDATA[toUser]]></ToUserName><FromUserName><![CDATA[FromUser]]></FromUserName><CreateTime>123456789</CreateTime><MsgType><![CDATA[event]]></MsgType><Event><![CDATA[CLICK]]></Event><EventKey><![CDATA[EVENTKEY]]></EventKey></xml>";
		ClickEventMessage msg = new ClickEventMessage();
		msg.loadDataFromXml(xmlStr);
		System.out.println(msg);
	}
	
	@Test
	public void testToStr(){
		ClickEventMessage msg = new ClickEventMessage();
		msg.setCreateTime(System.currentTimeMillis());
		msg.setMsgType(MsgType.event);
		msg.setEvent(Event.CLICK);
		msg.setEventKey("eventKey");
		msg.setFromUserName("long");

		msg.setMsgId("1111111111111");
		msg.setToUserName("zhu zhu");
		
		System.out.println(msg.toXmlMsg());
	}

}
