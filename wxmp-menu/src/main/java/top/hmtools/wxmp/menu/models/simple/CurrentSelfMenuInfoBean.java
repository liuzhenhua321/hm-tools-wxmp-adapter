package top.hmtools.wxmp.menu.models.simple;

/**
 * 自定义菜单配置
 * @author Hybomyth
 *
 */
public class CurrentSelfMenuInfoBean {

	/**
	 * 菜单是否开启，0代表未开启，1代表开启
	 */
	private Integer is_menu_open;
	
	/**
	 * 菜单信息
	 */
	private SelfmenuInfoBean selfmenu_info;

	/**
	 * 菜单是否开启，0代表未开启，1代表开启
	 * @return
	 */
	public Integer getIs_menu_open() {
		return is_menu_open;
	}

	/**
	 * 菜单是否开启，0代表未开启，1代表开启
	 * @param is_menu_open
	 */
	public void setIs_menu_open(Integer is_menu_open) {
		this.is_menu_open = is_menu_open;
	}

	/**
	 * 菜单信息
	 * @return
	 */
	public SelfmenuInfoBean getSelfmenu_info() {
		return selfmenu_info;
	}

	/**
	 * 菜单信息
	 * @param selfmenu_info
	 */
	public void setSelfmenu_info(SelfmenuInfoBean selfmenu_info) {
		this.selfmenu_info = selfmenu_info;
	}

	@Override
	public String toString() {
		return "CurrentSelfMenuInfoBean [is_menu_open=" + is_menu_open + ", selfmenu_info=" + selfmenu_info + "]";
	}
	
	
}
