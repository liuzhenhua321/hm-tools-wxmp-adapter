#### 前言
同微信服务器交互，是以http方式交互，总体为主动请求微信服务器获取反馈数据、被动接收微信服务器发送的数据并处理之后反馈数据至微信服务器。即：
- 主动请求微信服务器
	- get方式
	  - 获取的数据类型为json
	  - 获取的数据类型为 字节流（比如素材管理中的获取图片素材接口）
	- post方式
	  - 请求参数数据类型为json，获取反馈的数据类型为json
	  - 请求参数数据类型为form表单，获取反馈的数据类型为json （比如素材管理中上传图片素材接口）
- 被动接收微信服务器的请求，接收到的数据类型均为xml

#### 概述
本组件包是整个工具包的核心包，主要提供的功能有：
1. 全局配置信息文件管理
2. access token 获取、存储、扩展。
3. 基于动态代理机制（借鉴mybatis）以http请求方式主动请求微信服务器API接口交互数据。
4. 基于反射、路由机制（借鉴spring MVC）解析微信服务器事件消息xml格式文本数据并根据其消息类别选择对应得处理器处理。

#### 基本使用示例
```
//1 设置全局配置类信息并获取对象实例
WxmpConfiguration wxmpConfiguration = new WxmpConfiguration();
wxmpConfiguration.setAppid(AppId.appid); //必须，设置自己的在微信公众号的appid
wxmpConfiguration.setAppsecret(AppId.appsecret); // 必须，设置自己的在微信公众号的appsecret
wxmpConfiguration.setAccessTokenHandleClass(MyAccessTokenHandleClass.class);	//可选，设置自己的 access token 存储、读取（可以说是缓存）方案，可以用来实现分布式获取 access token 的方案。

//可选 获取access token
MyAccessTokenHandleClass accessTokenHandle = (MyAccessTokenHandleClass)wxmpConfiguration.getAccessTokenHandle(); //获取access token工具类，如果有自己实现，并在配置文件中配置了，可以转换为自己的实现类实例
AccessTokenBean accessToken = accessTokenHandle.getAccessToken();	//获取 access token 对象实例，已经实现过期则自动重新获取

//2 获取session工厂创建类的对象实例
WxmpSessionFactoryBuilder builder = new WxmpSessionFactoryBuilder();

//3 获取session工厂对象实例
WxmpSessionFactory factory = builder.build(wxmpConfiguration);

//4 获取session
WxmpSession wxmpSession = factory.openSession();

//5 获取请求微信api的接口对象实例，基于动态代理实现，参照mybatis设计思路，示例是 “自定义菜单”接口。
IMenuApi menuApi = wxmpSession.getMapper(IMenuApi.class);
```

#### 配置核心类
1. 类全限定路径：top.hmtools.wxmp.core.configuration.WxmpConfiguration
2. 配置项说明：  

属性名称 |  说明  
-|-
appid | 在微信注册的微信公众号应用主键id
appsecret | 在微信注册的微信公众号应用秘钥
charset | 收、发数据时的字符编码，缺省 utf-8
eUrlServer | 微信服务器地址
httpMaxTotal | httpclient 连接池配置参数
httpDefaultMaxPerRoute | httpclient 连接池配置参数
accessTokenHandleClass | 访问微信接口时所需的accessToken工具类名
forceAccessTokenString | 强制使用的accessToken字符串，当本属性被设置为不为空，且不为空字符串时，则强制使用该accessToken字符串，以便于使用某些场景


#### access token 工具核心类
1. 核心类全限定路径：top.hmtools.wxmp.core.access_handle.BaseAccessTokenHandle
2. 缺省的access token本地缓存方案实现类全限定路径：top.hmtools.wxmp.core.access_handle.DefaultAccessTokenHandle
3. 如何获取
```
//1 设置全局配置类信息并获取对象实例
WxmpConfiguration wxmpConfiguration = new WxmpConfiguration();
wxmpConfiguration.setAppid(AppId.appid); //必须，设置自己的在微信公众号的appid
wxmpConfiguration.setAppsecret(AppId.appsecret); // 必须，设置自己的在微信公众号的appsecret
wxmpConfiguration.setAccessTokenHandleClass(MyAccessTokenHandleClass.class);	//可选，设置自己的 access token 存储、读取（可以说是缓存）方案，可以用来实现分布式获取 access token 的方案。

//2 获取access token handle
MyAccessTokenHandleClass accessTokenHandle = (MyAccessTokenHandleClass)wxmpConfiguration.getAccessTokenHandle();
```
4. 主要功能：
- `getAccessToken`  获取可用的 access token对象实例，这里的“可用”是指程序会根据当前access token是否过期而自动从微信服务器处获取，目前不包括因其它原因（比如另一个程序使用同一套appid将微信服务器处的access token刷新了）造成access token失效而自动重新获取。
- `getAccessTokenString`  获取可用的 access token 字符串，本方法本质是调用 `getAccessToken`。
- `getAccessTokenDirectFromWxServer`  完全独立的直接从微信服务器处获取access token，不会从缓存读取数据也不会将数据缓存，仅仅用于某些特殊场景测试使用。
- `refreshAccessToken`  刷新 AccessToken，重新获取AccessToken后，会写入外部存储
- `saveAccessTokenBeanToStorage`	将获取的 accessToken 信息存入外部存储，须自行实现，结合`getAccessTokenBeanFromStorage`实现access token 的本地存储方案、分布式存储方案
- `getAccessTokenBeanFromStorage` 	从外部存储获取accessToken，须自行实现，结合`saveAccessTokenBeanToStorage`实现access token 的本地存储方案、分布式存储方案

#### 主动http请求微信服务器
1. 核心类全限定路径：top.hmtools.wxmp.core.DefaultWxmpSession
2. 暂时不提供自定义实现类
3. 本核心类基于httpclient连接池、动态代理（参照mybatis）实现。直接使用`top.hmtools.wxmp.core.DefaultWxmpSession`中诸如`httppost**`、`httpget**`之类的方法是对 httpclient 的再次封装，即直接发送请求到参数中指定的URL地址并获取反馈数据。`getMapper`是很核心的功能方法，通过该方法获取访问微信api接口的动态代理类对象实例（参照mybatis实现），要求其接口被`@WxmpApi`注解修饰类，被`@WxmpApi`注解修饰方法。获取的对象实例，请求微信api接口时均会自动带上 access token参数，使用时只需编写好请求参数、返回结果数据结构类即可。具体实现参见`top.hmtools.wxmp.core.DefaultWxmpSession.invoke(Object, Method, Object[])`。
4. 返回结果基础数据结构类：`top.hmtools.wxmp.core.model.ErrcodeBean`。创建自定义返回结果数据结构时，建议继承该类。

#### 被动解析并处理来自微信服务器的xml数据
1. 基础的消息类全限定路径：top.hmtools.wxmp.core.model.message.BaseMessage
2. 基础的事件通知消息类全限定路径：top.hmtools.wxmp.core.model.message.BaseEventMessage
3. 基础的消息类中已实现将Javabean与xml进行互转，基于 XStream 实现。
4. top.hmtools.wxmp.core.DefaultWxmpMessageHandle 实现了根据xml字符串数据，自动选择对应该消息的处理方法进行处理（参照spring MVC实现）。要求处理类必须被`@WxmpController`注解修饰类，被`@WxmpRequestMapping`注解修饰方法，且方法的形式参数必须是 top.hmtools.wxmp.core.model.message.BaseMessage 子类并被`@WxmpMessage`注解修饰类。具体处理实现参见 `top.hmtools.wxmp.core.DefaultWxmpMessageHandle.addMessageMetaInfo(Object)`与`top.hmtools.wxmp.core.DefaultWxmpMessageHandle.processXmlData(String)`。
5. 使用示例：
```
// 1 创建“路由”实例
DefaultWxmpMessageHandle defaultWxmpMessageHandle = new DefaultWxmpMessageHandle();

// 2 注册xml处理类（类似spring MVC 中的 controller）到“路由”中的容器
WxmpControllerTest wxmpControllerTest = new WxmpControllerTest();
defaultWxmpMessageHandle.addMessageMetaInfo(wxmpControllerTest);

// 3 （可选） 打印容器看看注册情况
System.out.println(JSON.toJSONString(this.defaultWxmpMessageHandle.getEventMap()));
System.out.println(JSON.toJSONString(this.defaultWxmpMessageHandle.getMsgTypeMap()));

// 4 解析并处理微信事件通知消息xml字符串
String xml = "<xml><ToUserName><![CDATA[toUser]]></ToUserName><FromUserName><![CDATA[fromUser]]></FromUserName><CreateTime>1348831860</CreateTime><MsgType><![CDATA[text]]></MsgType><Content><![CDATA[this is a test]]></Content><MsgId>1234567890123456</MsgId></xml>";
Object processXmlData = this.defaultWxmpMessageHandle.processXmlData(xml);
System.out.println(processXmlData);
```





















