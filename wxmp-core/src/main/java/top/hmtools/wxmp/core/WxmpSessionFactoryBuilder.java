package top.hmtools.wxmp.core;

import top.hmtools.wxmp.core.configuration.WxmpConfiguration;

public class WxmpSessionFactoryBuilder {

	/**
	 * 根据配置对象实例生成一个 微信公众号回话工厂
	 * @param wxmpConfiguration
	 * @return
	 */
	public WxmpSessionFactory build(WxmpConfiguration wxmpConfiguration){
		DefaultWxmpSessionFactory result = new DefaultWxmpSessionFactory(wxmpConfiguration);
		return result;
	}
}
