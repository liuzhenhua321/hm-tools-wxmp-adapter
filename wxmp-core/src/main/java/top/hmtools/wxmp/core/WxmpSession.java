package top.hmtools.wxmp.core;

import java.io.Closeable;
import java.io.InputStream;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;

public interface WxmpSession extends Closeable {

	/**
	 * post方式请求指定URL，请求传参为json字符串requestBody，服务器返回为json格式字符串，本地转换为JSONObject
	 * @param url
	 * @param jsonString
	 * @return
	 */
	public JSONObject httpPostInJsonBody(String url,String jsonString);
	
	/**
	 * post方式请求指定URL，请求传参为json字符串requestBody，服务器返回为json格式字符串，本地转换为JSONObject
	 * @param url
	 * @param obj
	 * @return
	 */
	public JSONObject httpPostInJsonBody(String url,Object obj);
	
	/**
	 * post方式请求指定URL，请求传参为json字符串requestBody，服务器返回为json格式字符串，本地转换为指定的Java对象实例
	 * @param url
	 * @param jsonString
	 * @param clazz
	 * @return
	 */
	public <T>T httpPostInJsonBody(String url,String jsonString,Class<T> clazz);
	
	/**
	 * post方式请求指定URL，请求传参为json字符串requestBody，服务器返回为json格式字符串，本地转换为指定的Java对象实例
	 * @param url
	 * @param obj
	 * @param clazz
	 * @return
	 */
	public <T>T httpPostInJsonBody(String url,Object obj,Class<T> clazz);
	
	/**
	 * get方式请求指定URL，请求传参为url参数，服务器返回为json格式字符串，本地转换为JSONObject
	 * @param url
	 * @return
	 */
	public JSONObject httpGet(String url);
	
	/**
	 * get方式请求指定URL，请求传参为url参数，服务器返回为json格式字符串，本地转换为inputStream
	 * @param url
	 * @param params
	 * @return
	 */
	public InputStream httpGetAsInputStream(String url, Map<String, String> params);

	/**
	 * get方式请求指定URL，请求传参为url参数，服务器返回为json格式字符串，本地转换为指定的Java对象实例
	 * @param url
	 * @param clazz
	 * @return
	 */
	public <T>T httpGet(String url,Class<T> clazz);
	
	/**
	 * get方式请求指定URL，请求传参为url参数，服务器返回为json格式字符串，本地转换为指定的Java对象实例
	 * @param url
	 * @param params
	 * @param clazz
	 * @return
	 */
	public <T>T httpGet(String url,Map<String, String> params,Class<T> clazz);
	
	/**
	 * post方式请求指定URL，请求传参为form表单requestBody，服务器返回为json格式字符串，本地转换为JSONObject
	 * @param url
	 * @param params
	 * @return
	 */
	public JSONObject httpPostInFormBody(String url,Map<String, Object> params);
	
	/**
	 * post方式请求指定URL，请求传参为form表单requestBody，服务器返回为json格式字符串，本地转换为指定的Java对象实例
	 * @param url
	 * @param params
	 * @param clazz
	 * @return
	 */
	public <T>T httpPostInFormBody(String url,Map<String, Object> params,Class<T> clazz);
	
	/**
	 * 获取被动态代理的对象实例，传入的接口必须被 @WxmpApi 注解类，被 @WxmpMapper 注解方法
	 * @param type
	 * @return
	 */
	public <T>T getMapper(Class<T> type);
}
