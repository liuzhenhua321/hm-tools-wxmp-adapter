package top.hmtools.wxmp.core.model.message;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import top.hmtools.wxmp.core.model.message.enums.Event;

/**
 * 基本的事件消息
 * @author Hybomyth
 *
 */
public abstract class BaseEventMessage extends BaseMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4207716568213821357L;

	/**
	 * 事件类型
	 */
	@XStreamAlias("Event")
	protected Event event;
	
	/**
	 * 事件KEY值
	 */
	@XStreamAlias("EventKey")
	protected String eventKey;

	/**
	 * 事件类型
	 * @return
	 */
	public Event getEvent() {
		return event;
	}

	/**
	 * 事件类型
	 * @param event
	 */
	public void setEvent(Event event) {
		this.event = event;
	}
	
	/**
	 * 事件类型
	 * @param event
	 */
	public void setEvent(String eventStr) {
		if(eventStr==null||eventStr.length()<1){
			return;
		}
		
		Event[] values = Event.values();
		for(Event ee:values){
			if(ee.getName().equals(event)){
				this.event=ee;
			}
		}
	}

	/**
	 * 事件KEY值
	 * @return
	 */
	public String getEventKey() {
		return eventKey;
	}

	/**
	 * 事件KEY值
	 * @param eventKey
	 */
	public void setEventKey(String eventKey) {
		this.eventKey = eventKey;
	}
	
	
}
