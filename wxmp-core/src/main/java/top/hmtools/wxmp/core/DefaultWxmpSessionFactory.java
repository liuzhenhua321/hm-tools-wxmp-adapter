package top.hmtools.wxmp.core;

import top.hmtools.wxmp.core.configuration.WxmpConfiguration;

public class DefaultWxmpSessionFactory implements WxmpSessionFactory {
	
	private final WxmpConfiguration wxmpConfiguration;
	
	public DefaultWxmpSessionFactory(WxmpConfiguration wxmpConfiguration) {
		this.wxmpConfiguration = wxmpConfiguration;
	}

	/**
	 * 获取全局的配置信息对象实例
	 * @return
	 */
	@Override
	public WxmpConfiguration getWxmpConfiguration() {
		return this.wxmpConfiguration;
	}

	@Override
	public WxmpSession openSession() {
		DefaultWxmpSession wxmpSession = new DefaultWxmpSession(this.wxmpConfiguration);
		return wxmpSession;
	}

}
