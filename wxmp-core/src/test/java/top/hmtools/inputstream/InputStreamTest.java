package top.hmtools.inputstream;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

public class InputStreamTest {

	@Test
	public void testHashcode() throws IOException{
		InputStream inputStream = IOUtils.toInputStream("hello,hybo","utf-8");
		
		System.out.println(inputStream.hashCode());
		String string = IOUtils.toString(inputStream,"utf-8");
		System.out.println(string);
		System.out.println(inputStream.hashCode());
		string = IOUtils.toString(inputStream,"utf-8");
		System.out.println(string);
	}
}
