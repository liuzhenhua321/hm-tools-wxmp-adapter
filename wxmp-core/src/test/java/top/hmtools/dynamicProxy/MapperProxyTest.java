package top.hmtools.dynamicProxy;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class MapperProxyTest {

	@Test
	public void testAaa(){
		MapperProxy mp = new MapperProxy();
		IAaaMapper aaaMapper = mp.newInstance(IAaaMapper.class);
		Map<String, String> doAaa = aaaMapper.doAaa("aaaaa", 22, new HashMap<String,String>());
		System.out.println(doAaa);
	}
}
